use fmt;
use format::json::token;
use io;
use misc;
use os;
use strio;

//This is not illegal per the json spec, but this is an extension.
@test fn fixed_lex() void = {
	let basic_json = "{ 'key' : 'val' }";
	let expect: []str = ["{", "key", ":", "val", "}"];

	let got = match (lex(basic_json)) {
	case error =>
		fmt::fatal("Unexpected error while [lex].");
	case let g: *token::list =>
		yield g;
	};

	let first = token::get(got, 0): *token::token;
	assert(first.type_ == "syntax");

	let second = token::get(got, 1): *token::token;
	assert(second.type_ == "string");

	assert(assert_str_slice_is_equal(got, expect));
	free(got);
};

@test fn str_lex() void = {
	let basic_json = "{'key':'fubar'}";
	let expect: []str = ["{", "key", ":", "fubar", "}"];

	let got = match (lex(basic_json)) {
	case error =>
		fmt::fatal("Unexpected error while [lex].");
	case let g: *token::list =>
		yield g;
	};

	assert(assert_str_slice_is_equal(got, expect));
	free(got);
};

@test fn str_lex_with_double_quote() void = {
	let basic_json = "{\"key\":\"fubar\"}";
	let expect: []str = ["{", "key", ":", "fubar", "}"];

	let got = match (lex(basic_json)) {
	case error =>
		fmt::fatal("Unexpected error while [lex].");
	case let g: *token::list =>
		yield g;
	};

	assert(assert_str_slice_is_equal(got, expect));
	free(got);
};

@test fn number_lex() void = {
	let basic_json = "{'key': 42}";
	let expect: []str = ["{", "key", ":", "42", "}"];

	let got = match (lex(basic_json)) {
	case error =>
		fmt::fatal("Unexpected error while [lex].");
	case let g: *token::list =>
		yield g;
	};

	let fourth = token::get(got, 3): *token::token;
	assert(fourth.type_ == "number");

	assert(assert_str_slice_is_equal(got, expect));
	free(got);
};

@test fn null_lex() void = {
	let basic_json = "{'key': null}";
	let expect: []str = ["{", "key", ":", "null", "}"];

	let got = match (lex(basic_json)) {
	case error =>
		fmt::fatal("Unexpected error while [lex].");
	case let g: *token::list =>
		yield g;
	};

	let fourth = token::get(got, 3): *token::token;
	assert(fourth.type_ == "litteral");

	assert(assert_str_slice_is_equal(got, expect));
	free(got);
};

@test fn true_lex() void = {
	let basic_json = "{'key': true}";
	let expect: []str = ["{", "key", ":", "true", "}"];

	let got = match (lex(basic_json)) {
	case error =>
		fmt::fatal("Unexpected error while [lex].");
	case let g: *token::list =>
		yield g;
	};

	assert(assert_str_slice_is_equal(got, expect));
	free(got);
};

@test fn false_lex() void = {
	let basic_json = "{'key': false}";
	let expect: []str = ["{", "key", ":", "false", "}"];

	let got = match (lex(basic_json)) {
	case error =>
		fmt::fatal("Unexpected error while [lex].");
	case let g: *token::list =>
		yield g;
	};

	assert(assert_str_slice_is_equal(got, expect));
	free(got);
};

@test fn garbage_raises() void = {
	let basic_json = "{'key' : garbage}";

	match (lex(basic_json)) {
	case *token::list =>
		assert(false, "There should be an error.");
	case error =>
		void;
	};
};

fn assert_str_slice_is_equal(got: *token::list, s2: []str) bool = {
	if (token::length(got) != len(s2)) {
		fmt::errorf("Slice size are not the same. s1: {}, s2: {} ", token::length(got), len(s2))!;
		return false;
	};
	let s1: []str = alloc([]);

	let iter = &token::iter(got);
	for (true) {
		let token_: *token::token = match (token::next(iter)) {
		case void =>
			break;
		case let t: *token::token =>
			yield t;
		};
		append(s1, token_.value);
	};

	let same = true;
	for (let i = 0z; i < len(s1); i += 1) {
		if (s1[i] != s2[i]) {
			fmt::errorf("Slice element are different. s1[{}]: {}, s2[{}]: {} ", i, s1[i], i, s2[i])!;
			same = false;
		};
	};

	if (!same) {
		fmt::errorf("Got: {}, Expect: {} ", print_str_slice(s1), print_str_slice(s2))!;
	};
	return same;
};

fn print_str_slice(s: []str) str = {
	let out: *strio::stream = &strio::dynamic();
	misc::write_str(out, "[");
	for (let i = 0z; i < len(s); i += 1) {
		if (i != 0z) {
			misc::write_str(out, ", ");
		};
		misc::write_str(out, "\"");
		misc::write_str(out, s[i]);
		misc::write_str(out, "\"");
	};
	misc::write_str(out, "]");
	return strio::string(out);
};
